import { Noticia } from "../interfaces";

/** Dummy user data. */
export const sampleUserData: Noticia[] = [
  {
    id: 101,
    titulo: "Alice",
    imagenPrincipal: "/uploads/default.jpg",
    fechaPublicacion: "2024-01-15T13:00:00.000Z",  // Convertir la fecha a una cadena
    lugar: "Lugar 1",
    autor: "Autor 1",
    contenido: "Contenido de la noticia 1"
  },


];
