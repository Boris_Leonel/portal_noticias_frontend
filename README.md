# Portal de Noticias El Hocicón - Backend

Bienvenido al frontend del Portal de Noticias "El Hocicón". Este proyecto proporciona la vista usuario para gestionar y ofrecer noticias a través de una conexion API RESTful.

## Funcionalidades

- Gestión de noticias: Crear, leer, actualizar y eliminar noticias.
- Carga de imágenes para las noticias.
- Búsqueda y ordenación de noticias.

## Tecnologías Utilizadas

- Node.js
- NextJS (Framework para Node.js)
- CSS: MUI (Para las vistas)

## Instalación

Antes de comenzar, asegúrate de tener [Node.js](https://nodejs.org/) instalados en tu sistema.

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/Boris_Leonel/portal_noticias_frontend.git
   cd portal_noticias_frontend

2. **Instala las dependencias:**

   ```bash
   npm install

3. **Configura la url de la API:**
Ingresar al archivo utils/constants.ts
   ```bash
   // Reemplaza esto con la URL real de tu servidor
   export const SERVER_URL = "http://localhost:3000"


4. **Inicia el servidor:**
   ```bash
   npm run dev

