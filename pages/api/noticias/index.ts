import { NextApiRequest, NextApiResponse, GetStaticProps } from "next";

import { sampleUserData } from "../../../utils/sample-data";
import { SERVER_URL } from "../../../utils/constants";



const handler = async (_req: NextApiRequest, res: NextApiResponse) => {
  try {
    const noticias = await getNoticias();
    res.status(200).json({ noticias });  // Envia el cuerpo de la respuesta como un objeto JSON
  } catch (err: any) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};


export default handler;

export const getNoticias = async () => {
  try {
    const res = await fetch(`${SERVER_URL}/noticias`);

    if (!res.ok) {
      throw new Error("No se pudo obtener la lista de noticias");
    }

    const data = await res.json();
    return data;
  } catch (error) {
    console.error("Error en getNoticias:", error.message);
    return [];
  }
};
