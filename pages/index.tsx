// pages/users/index.tsx
import { useState } from "react";
import { GetStaticProps } from "next";
import Link from "next/link";
import { Typography, Button, Container, Grid, TextField, Radio, RadioGroup, FormControlLabel, FormControl, FormLabel } from "@mui/material";
import { styled } from "@mui/system";
import { Noticia } from "../interfaces";
import Layout from "../components/Layout";
import List from "../components/List";
import { useEffect } from "react";
import { SERVER_URL } from "../utils/constants";

type Props = {
  noticias: Noticia[];
};

const CustomContainer = styled(Container)({
  textAlign: "center",
  marginTop: "10px",
});




const WithStaticProps = ({ noticias: initialNoticias }: Props) => {
  const [noticias, setNoticias] = useState<Noticia[]>(initialNoticias);

  

  


  return (
    <Layout title="EL HOCICÓN - Portal de Noticias">
      <Grid container spacing={1}>
        <Grid item xs={12} md={3}>
          
        </Grid>
        
          <List noticias={noticias} />
        
      </Grid>

    </Layout>
  );
};

export const getServerSideProps: GetStaticProps = async ({ preview = false }) => {
  try {
    const res = await fetch(`${SERVER_URL}/noticias`);

    if (!res.ok) {
      throw new Error("No se pudo obtener la lista de noticias");
    }

    const data = await res.json();

    const noticias: Noticia[] | undefined = data;

    if (!Array.isArray(noticias)) {
      throw new Error("La respuesta no es un array de noticias");
    }

    return { props: { noticias: noticias || [] } };
  } catch (error) {
    console.error("Error en getServerSideProps:", error.message);

    return { props: { noticias: [] } };
  }
};

export default WithStaticProps;
