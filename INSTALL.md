## Instalación

Antes de comenzar, asegúrate de tener [Node.js](https://nodejs.org/) instalados en tu sistema.

1. **Clona el repositorio:**

   ```bash
   git clone https://gitlab.com/Boris_Leonel/portal_noticias_frontend.git
   cd portal_noticias_frontend

2. **Instala las dependencias:**

   ```bash
   npm install

3. **Configura la url de la API:**
Ingresar al archivo utils/constants.ts
   ```bash
   // Reemplaza esto con la URL real de tu servidor
   export const SERVER_URL = "http://localhost:3000"


4. **Inicia el servidor:**
   ```bash
   npm run dev
