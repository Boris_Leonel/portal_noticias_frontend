import React, { ReactNode } from "react";
import Link from "next/link";
import Head from "next/head";
import { AppBar, Container, CssBaseline, Toolbar,Grid, Typography } from "@mui/material";
import styles from "./Layout.module.css";

type Props = {
  children?: ReactNode;
  title?: string;
};

const Layout = ({ children, title = "This is the default title" }: Props) => (
  <div style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <CssBaseline />
    <AppBar position="static" sx={{ backgroundColor: '#f8f9fa', color: "#fff", textDecoration: "none" }}>
      <Toolbar className={styles["toolbar"]}>
      <Grid container spacing={2} justifyContent="center">
          {/* <Grid item>
            <Link href="/" passHref>
              <Typography variant="h6" className={styles["navbar-link"]}>
                Home
              </Typography>
            </Link>
          </Grid> */}
          {/* <Grid item>
            <Link href="/about" passHref>
              <Typography variant="h6" className={styles["navbar-link"]}>
                About
              </Typography>
            </Link>
          </Grid> */}
          <Grid item>
            <Link href="/" passHref>
              <Typography variant="h6" className={styles["navbar-link"]}>
                Noticias
              </Typography>
            </Link>
          </Grid>
          <Grid item>
            <Link href="/api/noticias" passHref>
              <Typography variant="h6" className={styles["navbar-link"]}>
                Noticias API
              </Typography>
            </Link>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
    <Grid container justifyContent="center" style={{ flex: 1 }}>
      <Grid item xs={12} sm={10} md={8}>
        <Container component="main" sx={{ mt: 4 }}>
          {children}
        </Container>
      </Grid>
    </Grid>
    <footer style={{ backgroundColor: "#f8f9fa", color: "black", padding: 10, textAlign: "center" }}>
      <hr style={{ borderColor: "#fff" }} />
      <span>Hecho por Boris Leonel 👌</span>
    </footer>
  </div>
);

export default Layout;
