//components/List.tsx
import React, { useState } from "react";
import { Input, Card, CardContent, CardMedia, Modal, Typography, Button, Grid, useMediaQuery, useTheme, TextField, FormControl, FormLabel, RadioGroup, FormControlLabel, Radio } from "@mui/material";
import { format } from "date-fns";
import { SERVER_URL } from "../utils/constants";
import { Noticia } from "../interfaces";
import IconButton from "@mui/material/IconButton";
import DeleteIcon from "@mui/icons-material/Delete";

type Props = {
  noticias: Noticia[];
};

const List = ({ noticias: initialNoticias }: Props) => {
  const [noticias, setNoticias] = useState<Noticia[]>(initialNoticias);

  const [selectedNoticia, setSelectedNoticia] = useState<Noticia | null>(null);
  const [selectedNoticia2, setSelectedNoticia2] = useState<Noticia | null>(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [orderBy, setOrderBy] = useState("fechaPublicacion");
  const [order, setOrder] = useState("DESC");

  const [isEditing, setIsEditing] = useState(false);
  const theme = useTheme();
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [formData, setFormData] = useState({
    titulo: "",
    imagenPrincipal: "",
    image: null,
    lugar: "",
    autor: "",
    contenido: "",
    fechaPublicacion: "",
  });
  const fetchNoticias = async () => {
    try {
      const res = await fetch(`${SERVER_URL}/noticias`);

      if (!res.ok) {
        throw new Error("No se pudo obtener la lista de noticias");
      }

      const data = await res.json();

      const updatedNoticias: Noticia[] | undefined = data;

      if (!Array.isArray(updatedNoticias)) {
        throw new Error("La respuesta no es un array de noticias");
      }

      setNoticias(updatedNoticias || []);
    } catch (error) {
      console.error("Error al actualizar noticias:", error.message);
    }
  };
  const fetchData = async () => {
    try {
      const url = `${SERVER_URL}/noticias/buscar?searchTerm=${searchTerm}&orderBy=${orderBy}&order=${order}`;
      const res = await fetch(url);

      if (!res.ok) {
        throw new Error("No se pudo obtener la lista de noticias");
      }

      const data = await res.json();
      console.log(data);
      const updateNoticias: Noticia[] | undefined = data;

      if (!Array.isArray(updateNoticias)) {
        throw new Error("La respuesta no es un array de noticias");
      }

      setNoticias(updateNoticias || []);
    } catch (error) {
      console.error("Error al actualizar noticias:", error.message);
    }
  };
  const handleOpenModal = (noticia: Noticia) => {
    setSelectedNoticia(noticia);
  };
  const handleOpenModal2 = (noticia: Noticia, editing: boolean) => {
    setSelectedNoticia2(null); // Limpiar el primer modal
    setSelectedNoticia2(noticia);
    setIsEditing(editing);

    // Llenar el formulario con los datos de la noticia seleccionada
    setFormData({
      titulo: noticia.titulo,
      imagenPrincipal: noticia.imagenPrincipal,
      image: null,
      lugar: noticia.lugar,
      autor: noticia.autor,
      contenido: noticia.contenido,
      fechaPublicacion: format(new Date(noticia.fechaPublicacion), "yyyy-MM-dd'T'HH:mm"),
    });
  };
  const handleCloseModal = () => {
    setSelectedNoticia(null);
  };
  const handleCloseModal2 = () => {
    setSelectedNoticia2(null);
    setIsEditing(false);
    // Limpiar el formulario al cerrar el modal
    setFormData({
      titulo: "",
      imagenPrincipal: "",
      lugar: "",
      image: null,
      autor: "",
      contenido: "",
      fechaPublicacion: "",
    });
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    // Actualizar el estado del formulario cuando se cambian los valores
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async () => {
    try {
      const formDataToSend = new FormData();
      // Agregar datos al formulario
      for (const key in formData) {
        formDataToSend.append(key, formData[key]);
      }

      // // Agregar la imagen al formulario (si está presente)
      // if (formData.image instanceof File) {
      //   formDataToSend.append('image', formData.image);
      // }

      const url = `${SERVER_URL}/noticias${isEditing ? `/${selectedNoticia2?.id}` : ''}`;
      console.log('Datos del formulario:', url);

      // Mostrar todos los atributos de formDataToSend
      // console.log('Datos del formulario:');
      // for (const entry of formDataToSend.entries()) {
      //   console.log(entry);
      // }

      const response = await fetch(url, {
        method: isEditing ? 'PUT' : 'POST',
        body: formDataToSend,

      });

      if (response.ok) {
        console.log('Formulario enviado exitosamente');
        handleCloseModal2();
        fetchNoticias();

      } else {
        console.error('Error al enviar el formulario');
      }
    } catch (error) {
      console.error('Error al enviar el formulario:', error);
    }
  };
  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setFormData((prevData) => ({
      ...prevData,
      image: file,
    }));
  };
  const formattedDate = selectedNoticia
    ? format(new Date(selectedNoticia.fechaPublicacion), "dd - MMM - yyyy HH:mm")
    : "";

  const handleCreateNoticia = () => {
    const newNoticia: Noticia = {
      id: 0, // Otra lógica para generar un ID único
      titulo: "",
      imagenPrincipal: "",
      lugar: "",
      autor: "",
      contenido: "",
      fechaPublicacion: "2024-01-16T13:00:00.000Z",
    };

    handleOpenModal2(newNoticia, false);
  };

  const handleDelete = async () => {
    try {
      // Lógica para enviar la solicitud de eliminación al servidor
      const url = `${SERVER_URL}/noticias/${selectedNoticia2?.id}`;
      const response = await fetch(url, {
        method: 'DELETE',
      });

      if (response.ok) {
        console.log('Noticia eliminada exitosamente');
        handleCloseModal2();
        fetchNoticias();

      } else {
        console.error('Error al eliminar la noticia');
      }
    } catch (error) {
      console.error('Error al eliminar la noticia:', error);
    }
  };

  return (

    <Grid container spacing={2}>
      <Grid item xs={12} md={3}>

        {/* Barra lateral con textbox de búsqueda y opciones de clasificación */}
        <FormControl component="fieldset">
          <FormLabel component="legend">Buscar y Clasificar</FormLabel>
          <TextField
            label="Buscar"
            variant="outlined"
            fullWidth
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <RadioGroup
            aria-label="orderBy"
            name="orderBy"
            value={orderBy}
            onChange={(e) => setOrderBy(e.target.value)}
          >
            <FormControlLabel value="titulo" control={<Radio />} label="Título" />
            <FormControlLabel value="lugar" control={<Radio />} label="Lugar" />
            <FormControlLabel value="autor" control={<Radio />} label="Autor" />
            <FormControlLabel value="fechaPublicacion" control={<Radio />} label="Fecha de Publicación" />
          </RadioGroup>
          <hr style={{ borderColor: "#00000", backgroundColor: "#f8f9fa" }} />
          <RadioGroup
            aria-label="order"
            name="order"
            value={order}
            onChange={(e) => setOrder(e.target.value)}
          >
            <FormControlLabel value="ASC" control={<Radio />} label="Ascendente" />
            <FormControlLabel value="DESC" control={<Radio />} label="Descendente" />
          </RadioGroup>
          <Button onClick={fetchData}>Buscar y Ordenar</Button>

        </FormControl>
      </Grid>
      <Grid item xs={12} md={9}>
        <Grid container spacing={2}>
          <Button onClick={handleCreateNoticia}>Crear Noticia</Button>

          {noticias.map((noticia) => (
            <Grid item key={noticia.id} xs={12} sm={6} md={4} lg={3}>
              <Card style={{ maxWidth: "300px" }}>
                <CardMedia
                  component="img"
                  height="140"
                  image={`${SERVER_URL}${noticia.imagenPrincipal}`}
                  alt={noticia.titulo}
                />
                <CardContent>
                  <Typography variant="h6" component="div">
                    {noticia.titulo}
                  </Typography>
                  <Button onClick={() => handleOpenModal(noticia)}>Ver más</Button>
                  <Button onClick={() => handleOpenModal2(noticia, true)}>Editar</Button>
                </CardContent>
              </Card>
            </Grid>
          ))}
          <Modal open={!!selectedNoticia} onClose={handleCloseModal}>
            <div style={{ position: "absolute", top: 0, left: 0, width: "100%", height: "100%", backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
              <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100%" }}>
                <Card style={{ width: isSmallScreen ? "90%" : "70%", height: "90%", backgroundColor: "#fff", display: "flex", flexDirection: isSmallScreen ? "column" : "row" }}>
                  <CardMedia
                    component="img"
                    width={isSmallScreen ? "100%" : "200px"}
                    image={`${SERVER_URL}${selectedNoticia?.imagenPrincipal}`}
                    alt={selectedNoticia?.titulo}
                    style={{ objectFit: "cover", width: "75%" }}
                  />
                  <CardContent style={{ width: "100%", padding: "20px", display: "flex", flexDirection: "column" }}>
                    <Typography variant="h4" component="div" style={{ marginBottom: "10px", color: "#333", flexShrink: 0 }}>
                      {selectedNoticia?.titulo}
                    </Typography>
                    {!isSmallScreen && (
                      <Typography variant="h6" component="div" style={{ color: "#777", flexShrink: 0, fontSize: "15px" }}>
                        {formattedDate}
                      </Typography>
                    )}
                    <div style={{ flexGrow: 1, marginBottom: "10px" }}>
                      <Typography variant="h6" component="div" style={{ color: "#777", flexShrink: 0 }}>
                        Lugar: {selectedNoticia?.lugar}
                      </Typography>
                      <Typography variant="h6" component="div" style={{ color: "#777", flexShrink: 0 }}>
                        Autor: {selectedNoticia?.autor}
                      </Typography>
                      <Typography variant="body2" color="text.secondary" style={{ marginBottom: "10px", flexGrow: 1 }}>
                        {selectedNoticia?.contenido}
                      </Typography>
                    </div>
                    <Button onClick={handleCloseModal} style={{ marginTop: "10px", alignSelf: "flex-end" }}>
                      Cerrar
                    </Button>
                  </CardContent>
                </Card>
              </div>
            </div>
          </Modal>

          <Modal open={!!selectedNoticia2} onClose={handleCloseModal2}>
            <div style={{ position: "absolute", top: 0, left: 0, width: "100%", height: "100%", backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
              <div style={{ display: "flex", justifyContent: "center", alignItems: "center", height: "100%" }}>
                <form style={{ width: isSmallScreen ? "90%" : "70%", height: "90%", backgroundColor: "#fff", display: "flex", flexDirection: isSmallScreen ? "column" : "row" }}>
                  <div style={{ width: "100%", padding: "20px", display: "flex", flexDirection: "column" }}>
                    <Typography variant="h4" component="div" style={{ marginBottom: "10px", color: "#333", flexShrink: 0 }}>
                      {isEditing ? "Editar: " : "Crear: "}{selectedNoticia?.titulo}
                    </Typography>
                    {!isSmallScreen && (
                      <Typography variant="h6" component="div" style={{ color: "#777", flexShrink: 0, fontSize: "15px" }}>
                        {formattedDate}
                      </Typography>
                    )}
                    {isEditing && (
                      <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '10px' }}>
                        <IconButton onClick={handleDelete} color="error">
                          <DeleteIcon />
                        </IconButton>
                      </div>
                    )}
                    <div style={{ flexGrow: 1, marginBottom: "10px", padding: "10px" }}>
                      <TextField
                        name="titulo"
                        label="Título"
                        value={formData.titulo}
                        onChange={handleChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />
                      <Input
                        name="image"
                        type="file"
                        onChange={handleFileChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />

                      <TextField
                        name="lugar"
                        label="Lugar"
                        value={formData.lugar}
                        onChange={handleChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />
                      <TextField
                        name="autor"
                        label="Autor"
                        value={formData.autor}
                        onChange={handleChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />
                      <TextField
                        name="contenido"
                        label="Contenido"
                        multiline
                        rows={4}
                        value={formData.contenido}
                        onChange={handleChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />
                      <TextField
                        name="fechaPublicacion"
                        label="Fecha de Publicación"
                        type="datetime-local"
                        value={formData.fechaPublicacion}
                        onChange={handleChange}
                        style={{ marginBottom: "10px", padding: "20px" }}
                      />
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'flex-end', marginTop: '10px' }}>
                      <Button onClick={handleSubmit} style={{ marginRight: '10px' }}>
                        Enviar
                      </Button>
                      <Button onClick={handleCloseModal2}>
                        Cerrar
                      </Button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </Modal>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default List;
